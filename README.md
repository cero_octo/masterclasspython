# How to configure environment
* Check that python 3 is installed or install it
* Check that pip3 is installed or install it
* Go to application's root path and activate virtual environment
```shell
$ cd <application_path>
$ source venv/bin/activate
```
* Install packages with pip and requirements.txt
```shell
$ pip3 install -r requirements.txt
```

# How to test application
Go to application's root path and run pytest :
```shell
$ cd <application_path>
$ pytest tests
```

To run only some tests :
```shell
# Integration tests
$ pytest tests/integ

# Unit tests
$ pytest tests/units
```

# How to calculate coverage
* Check that pytest-cov is installed or install it
Go to application's root path and run pytest with coverage option :
```shell
$ cd <application_path>
$ pytest --cov=appli tests
```

To have more information (as seing lines covered or not), you should generate html report :
```shell
$ pytest --cov=appli tests --cov-report html
$ firefox ./htmlcov/index.html
```
 
